# PizzaShop

## Development
Project dependencies handled by [pipenv](https://github.com/pypa/pipenv)
```
pipenv install --dev
```

Create in source root file `local_settings.py`
with settings. You can set your own params.
```
SECRET_KEY = 'secret'
DEBUG = True
HOST = 'localhost'
ALLOWED_HOSTS = ['localhost']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'pizzashop',
        'USER': 'postgres',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': 5432,
    }
}
```

## Deployment
* set required params to `entrypoint.sh` now project works only with django `runserver`
* build project `docker build .` or alternatively use `docker-compose build web`


## Demo stand
(Docker)[https://www.docker.com/] and (docker-compose)[https://docs.docker.com/compose/install/] required
```bash
docker-compose build && docker-compose up -d
```
Then you will see **pizzashop** api by the link http://localhost:8000/api/


## Usecases
### Order pizza
* * It should be possible to specify the desired flavors of pizza (margarita, marinara, salami), the number of pizzas and their size (small, medium, large).*
    * create order with **POST** http://localhost:8000/api/orders/
    * get list of **Flavours** **GET** http://localhost:8000/api/flavours/
    * get list of **Sizes** **GET**  http://localhost:8000/api/flavours/
    * create order item with **POST** http://localhost:8000/api/items/ with id of flavour, size and count of pizzas

* *Track the status of delivery*
    * get list of **Actions** **GET** http://localhost:8000/api/history/ with id of **order**

* *It should be possible to order the same flavor of pizza but with different sizes multiple times*
    * you can pass **POST** http://localhost:8000/api/items/ as many times as you want, in case of the same `flavour` and `size`, `count` for specific combination will be updated to provided

### Update an order
* *It should be possible to update the details — flavours, count, sizes — of an order*
    * you can pass **POST** http://localhost:8000/api/items/ to update or create new pizza combination
    * you can delete **DELETE** http://localhost:8000/api/items/{item.id} to delete pizza combination
* *It should not be possible to update an order for some statutes of delivery (e.g. delivered).*
    * logic stored in `apps.orders.models.Order.is_locked` and triggers every update of order. This method also triggers by pizzas manipulations
    * now it is strict to change for `DELIVERY`, `DELETED` and `COMPLETED` orders.
* *It should be possible to change the status of delivery.*
    * * you can pass **POST** http://localhost:8000/api/orders/1/status/ with new status `{"status": 1}`.
    ```text
    NEW = 0, _('New')
    COOKING = 1, _('Cooking')
    DELIVERY = 2, _('Delivery')
    COMPLETED = 3, _('Completed')
    ERROR = 4, _('Error')
    DELETED = 5, _('Deleted')
  ```

### Remove an order
* trigger **DELETE** http://localhost:8000/api/orders/1/

###  Retrieve an order
* **GET** http://localhost:8000/api/orders/1/ to get order info
* **GET** http://localhost:8000/items/?order=1 to get ordered pizzas

### List orders
* * It should be possible to retrieve all the orders at once.*
    * trigger **GET** http://localhost:8000/api/orders/
* *Allow filtering by status / customer.*
    * trigger **GET** http://localhost:8000/api/orders/?client_name='vlad' also works with `client_name` `client_email` `client_phone` `status`
