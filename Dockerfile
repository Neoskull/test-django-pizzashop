# Pull a base image
FROM python:3.9-alpine
# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
#fix psycopg2
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
# Create a working directory for the django project
WORKDIR /code
# Copy requirements to the container
COPY Pipfile Pipfile.lock /code/
# Install the requirements to the container
RUN pip install pipenv && pipenv install --system
# Copy the project files into the working directory
COPY . /code/
# Open a port on the container
EXPOSE 8000
CMD ./entrypoint.sh