import os

SECRET_KEY = os.environ.get("DJANGO_SECRET_KEY")
DEBUG = int(os.environ.get("DJANGO_DEBUG", default=0))
HOST = os.environ.get('DJANGO_HOST')
ALLOWED_HOSTS = os.environ.get('DJANGO_ALLOWED_HOSTS', default='localhost').split(' ')

DATABASE_NAME = os.environ.get('DJANGO_DATABASE_NAME')
DATABASE_USER = os.environ.get('DJANGO_DATABASE_USER')
DATABASE_PASSWORD = os.environ.get('DJANGO_DATABASE_PASSWORD')
DATABASE_HOST = os.environ.get('DJANGO_DATABASE_HOST')
DATABASE_PORT = os.environ.get('DJANGO_DATABASE_PORT')


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DATABASE_NAME,
        'USER': DATABASE_USER,
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': DATABASE_HOST,
        'PORT': DATABASE_PORT,
    }
}

SENTRY_DSN = os.environ.get('SENTRY_DSN')
if SENTRY_DSN:
    import sentry_sdk
    from sentry_sdk.integrations.django import \
        DjangoIntegration

    sentry_sdk.init(
        SENTRY_DSN,
        integrations=[DjangoIntegration()]
)
