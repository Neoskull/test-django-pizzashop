
from rest_framework import (
    viewsets, serializers, mixins, decorators, response, exceptions
)
from django.utils.translation import gettext_lazy as _
from django.db.models import Sum

from apps.orders import models


class FlavourSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PizzaFlavour
        fields = ['id', 'title']


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PizzaSize
        fields = ['id', 'title']


class OrderItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.OrderItem
        fields = ['id', 'flavour', 'size', 'count', 'order']

    def create(self, validated_data):
        order = validated_data['order']
        order.is_locked()
        exists_instance = self.item_exists(validated_data)
        if exists_instance:
            return super().update(exists_instance, validated_data)
        return super().create(validated_data)

    def update(self, instance, validated_data):
        order = validated_data['order']
        order.is_locked()
        instance = self.item_exists(validated_data) or instance
        return super().update(instance, validated_data)

    def item_exists(self, validated_data):
        flavour = validated_data['flavour']
        size = validated_data['size']
        order = validated_data['order']
        instance = models.OrderItem.objects.filter(
            flavour=flavour, size=size, order=order
        ).first()

        return instance


class OrderSerializer(serializers.ModelSerializer):
    pizzas_count = serializers.SerializerMethodField()

    class Meta:
        model = models.Order
        fields = ['id', 'client_name', 'client_address',
                  'client_phone', 'extra', 'status', 'pizzas_count']
        read_only_fields = ('id', 'status')

    def update(self, instance, validated_data):
        instance.is_locked()
        return super().update(instance, validated_data)

    def get_pizzas_count(self, instance):
        # if not annotated, calc from model
        return getattr(instance, 'pizzas_count', None)\
              or instance.get_pizzas_count()


class OrderViewSet(viewsets.GenericViewSet, mixins.ListModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin, mixins.CreateModelMixin):
    serializer_class = OrderSerializer
    queryset = models.Order.objects.exclude(
        status=models.OrderStatus.DELETED
    ).prefetch_related('items').annotate(
        pizzas_count=Sum('items__count')
    )

    @decorators.action(methods=['GET', 'POST'], detail=True)
    def status(self, request, pk, **kwargs):
        instance = self.queryset.filter(pk=pk).first()
        if not instance:
            raise exceptions.NotFound()
        if request.method == 'POST':
            try:
                status = int(request.data['status'])
                if status not in models.OrderStatus:
                    raise ValueError
                if not instance.get_pizzas_count():
                    raise ValueError
            except (KeyError, ValueError):
                raise exceptions.ValidationError(
                    dict(status=[_('Invalid choice')])
                )
            instance.status = status
            instance.save()

        return response.Response(dict(id=instance.id, status=instance.status))


class OrderItemViewSet(viewsets.ModelViewSet):
    serializer_class = OrderItemSerializer
    queryset = models.OrderItem.objects
    filterset_fields = ['id', 'order']


class FlavoursViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin,
                      mixins.ListModelMixin):
    serializer_class = FlavourSerializer
    queryset = models.PizzaFlavour.objects
    filterset_fields = ['id', 'title']


class SizesViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin,
                      mixins.ListModelMixin):
    serializer_class = FlavourSerializer
    queryset = models.PizzaSize.objects
    filterset_fields = ['id', 'title']


class OrderActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OrdersAction
        fields = ['id', 'date', 'status', 'order']


class OrdersActionViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = OrderActionSerializer
    queryset = models.OrdersAction.objects
    filterset_fields = ['order', 'status']
