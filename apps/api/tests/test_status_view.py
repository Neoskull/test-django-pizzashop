import pytest

from django.urls import reverse
from apps.orders.models import Order, PizzaSize, PizzaFlavour, OrderItem

@pytest.fixture
def create_small_size():
    size = PizzaSize(title='small')
    size.save()
    return size


@pytest.fixture
def create_margarita_flavour():
    flav = PizzaFlavour(title='Margarita')
    flav.save()
    return flav


@pytest.fixture
def create_empty_order():
    order = Order(client_name='client_name', client_address='client_address',
                  client_phone='client_phone')
    order.save()
    return order


@pytest.fixture
def create_order_with_pizza(create_empty_order, create_small_size,
                            create_margarita_flavour):
    item = OrderItem(size=create_small_size, flavour=create_margarita_flavour,
                     count=1, order=create_empty_order)
    item.save()
    return create_empty_order


@pytest.mark.django_db
def test_fail_empty_order_status_change(client, create_empty_order):
    url = reverse('order-status', kwargs={'pk': create_empty_order.id})
    response = client.post(url, data=dict(status=1))
    assert response.status_code == 400


@pytest.mark.django_db
def test_success_order_status_change(client, create_order_with_pizza):
    url = reverse('order-status', kwargs={'pk': create_order_with_pizza.id})
    response = client.post(url, data=dict(status=1))
    assert response.status_code == 200
    assert response.data.get('status') == 1

    url = reverse('order-status', kwargs={'pk': create_order_with_pizza.id})
    response = client.get(url)
    assert response.status_code == 200
    assert response.data.get('status') == 1

