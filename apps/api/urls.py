from rest_framework import routers
from django.urls import path, include

from .views import (OrderViewSet, OrderItemViewSet, FlavoursViewSet,
                    SizesViewSet, OrdersActionViewSet)

router = routers.DefaultRouter()

router.register(r'orders', OrderViewSet)
router.register(r'items', OrderItemViewSet)
router.register(r'sizes', SizesViewSet)
router.register(r'flavours', FlavoursViewSet)
router.register(r'history', OrdersActionViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
