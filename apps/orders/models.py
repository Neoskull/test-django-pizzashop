from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from rest_framework import exceptions
from django.utils.translation import gettext_lazy as _


class PizzaFlavour(models.Model):
    title = models.CharField(verbose_name='Flavour', blank=False, null=False,
                               max_length=32)

    def __str__(self):
        return self.title


class PizzaSize(models.Model):
    title = models.CharField(verbose_name='Size', blank=False, null=False,
                            max_length=32)

    def __str__(self):
        return self.title


class OrderItem(models.Model):
    count = models.PositiveIntegerField(verbose_name='Count', default=1,
                                        validators=[
                                            MinValueValidator(1),
                                            MaxValueValidator(100)
                                        ])
    flavour = models.ForeignKey('PizzaFlavour', blank=False, null=False,
                                on_delete=models.PROTECT)
    size = models.ForeignKey('PizzaSize', blank=False, null=False,
                             on_delete=models.PROTECT)
    order = models.ForeignKey('Order', blank=False, null=False,
                              on_delete=models.CASCADE, related_name='items')


class OrderStatus(models.IntegerChoices):
    NEW = 0, _('New')
    COOKING = 1, _('Cooking')
    DELIVERY = 2, _('Delivery')
    COMPLETED = 3, _('Completed')
    ERROR = 4, _('Error')
    DELETED = 5, _('Deleted')


class OrdersAction(models.Model):
    order = models.ForeignKey('Order', blank=False, null=False,
                              on_delete=models.CASCADE,
                              related_name='history')
    date = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(verbose_name='Status',
                                 blank=False, null=False,
                                 choices=OrderStatus.choices,
                                 default=OrderStatus.NEW)


class Order(models.Model):
    STRICT_STATUSES = [
        OrderStatus.DELIVERY,
        OrderStatus.DELETED,
        OrderStatus.COMPLETED
    ]
    status = models.IntegerField(verbose_name='Status',
                                 blank=False, null=False,
                                 choices=OrderStatus.choices,
                                 default=OrderStatus.NEW)
    extra = models.TextField(verbose_name='Extra information', blank=True,
                             null=True, default='')
    client_name = models.CharField(verbose_name='Name', blank=False,
                                   null=False, max_length=60)
    client_address = models.CharField(verbose_name='Address', blank=True,
                                      null=True, max_length=120)
    client_phone = models.CharField(verbose_name='Phone', blank=False,
                                    null=False, max_length=30)

    def is_locked(self, raise_exception=True):
        locked = False
        if self.status in self.STRICT_STATUSES:
            locked = True
        if locked and raise_exception:
            raise exceptions.ValidationError(
                dict(id=[_('Order change is forbidden for this status')])
            )
        return locked

    def get_pizzas_count(self):
        agg = self.items.aggregate(models.Sum('count'))
        return agg.get('count__sum') or 0

    def __str__(self):
        return f'{self.id}-{self.status}'


def add_status_action(sender, instance, created, update_fields, **kwargs):
    fields = update_fields or dict()
    if created or 'status' in fields:
        OrdersAction(order=instance, status=instance.status).save()


models.signals.post_save.connect(add_status_action, sender=Order)
