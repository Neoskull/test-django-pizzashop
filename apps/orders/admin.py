from django.contrib import admin

from . import models


class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'status']


class PizzaFlavourAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']


class PizzaSizeAdmin(admin.ModelAdmin):
    list_display = ['id', 'title']


admin.site.register(models.Order, OrderAdmin)
admin.site.register(models.PizzaSize, PizzaSizeAdmin)
admin.site.register(models.PizzaFlavour, PizzaFlavourAdmin)